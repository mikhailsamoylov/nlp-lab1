import re


class CoNLLParser:

    @classmethod
    def parse(cls, input_filename, output_filename, with_tags=True):
        file_r = open(input_filename, encoding='utf-8')
        file_a = open(output_filename, 'a', encoding='utf-8')

        temp = file_r.readline()
        text_pattern = '# text = '
        word_tag_pattern = '^\\d+\\s+([\\w\\.,’:-]+)\\s+[\\w\\.,’:-]+\\s+([\\w]+)'
        while temp:
            if len(re.findall(text_pattern, temp)) > 0:
                output_string = ''
                while True:
                    temp = file_r.readline()
                    result = re.findall(word_tag_pattern, temp)

                    if len(result) > 0:
                        if with_tags:
                            output_string += result[0][0] + '/' + result[0][1] + ' '
                        else:
                            output_string += result[0][0] + ' '
                    else:
                        if temp == '\n':
                            output_string += '\n'
                            file_a.write(output_string)

                            break

            temp = file_r.readline()

        file_r.close()
        file_a.close()
