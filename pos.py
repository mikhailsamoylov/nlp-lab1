import solutionsB


def main():
    for corpus, paths in solutionsB.corpuses_path.items():
        for method in ['hmm_user_tagged_dev', 'crf_user_tagged_dev']:
            infile = open(paths[method], "r", encoding='utf-8')
            user_sentences = infile.readlines()
            infile.close()

            infile = open(paths['tagged_dev'], "r", encoding='utf-8')
            correct_sentences = infile.readlines()
            infile.close()

            num_correct = 0
            total = 0

            for user_sent, correct_sent in zip(user_sentences, correct_sentences):
                user_tok = user_sent.split()
                correct_tok = correct_sent.split()

                if len(user_tok) != len(correct_tok):
                    continue

                for u, c in zip(user_tok, correct_tok):
                    if u == c:
                        num_correct += 1
                    total += 1

            score = float(num_correct) / total * 100

            print("Percent correct tags for " + corpus + '/' + method, score)


if __name__ == "__main__":
    main()
