import nltk
import math
import time
import itertools

from CoNLLParser import CoNLLParser

START_SYMBOL = '*'
STOP_SYMBOL = 'STOP'
RARE_SYMBOL = '_RARE_'
RARE_WORD_MAX_FREQ = 5
LOG_PROB_OF_ZERO = -1000


# Receives a list of tagged sentences and processes each sentence to generate a list of words and a list of tags.
# Each sentence is a string of space separated "WORD/TAG" tokens, with a newline character in the end.
# Remember to include start and stop symbols in yout returned lists, as defined by the constants START_SYMBOL and
# STOP_SYMBOL.
# brown_words (the list of words) should be a list where every element is a list of the tags of a particular sentence.
# brown_tags (the list of tags) should be a list where every element is a list of the tags of a particular sentence.
def split_wordtags(brown_train):
    brown_words = []
    brown_tags = []

    for i, word_tag in enumerate(brown_train):
        brown_words.append([])
        brown_words[i].append(START_SYMBOL)
        brown_tags.append([])
        brown_tags[i].append(START_SYMBOL)
        word_tag_list = word_tag.split(' ')
        for word_and_tag in word_tag_list:
            word_and_tag_list = word_and_tag.split('/')
            if len(word_and_tag_list) == 2:
                brown_words[i].append(word_and_tag_list[0].lower())
                brown_tags[i].append(word_and_tag_list[1])
        brown_words[i].append(STOP_SYMBOL)
        brown_tags[i].append(STOP_SYMBOL)

    return brown_words, brown_tags


# This function takes tags from the training data and calculates tag trigram probabilities.
# It returns a python dictionary where the keys are tuples that represent the tag trigram, and the values
# are the log probability of that trigram
def calc_trigrams(brown_tags):
    bigram_count = {}
    trigram_count = {}

    for sentence in brown_tags:
        tokens_for_bigrams = sentence
        tokens_for_trigrams = [START_SYMBOL] + sentence + [STOP_SYMBOL]

        for bigram in nltk.bigrams(tokens_for_bigrams):
            if bigram not in bigram_count:
                bigram_count[bigram] = 1
            else:
                bigram_count[bigram] += 1

        for trigram in nltk.trigrams(tokens_for_trigrams):
            if trigram not in trigram_count:
                trigram_count[trigram] = 1
            else:
                trigram_count[trigram] += 1

    bigram_count[(START_SYMBOL, START_SYMBOL)] = len(brown_tags)

    trigram_prob = {}
    for i, j in trigram_count.items():
        trigram_prob[i] = math.log(float(j) / bigram_count[i[:2]], 2)
    return trigram_prob


# This function takes output from calc_trigrams() and outputs it in the proper format
def q2_output(q_values, filename):
    outfile = open(filename, "w", encoding='utf-8')
    trigrams = q_values.keys()

    # trigrams.sort()
    trigrams = sorted(trigrams)
    for trigram in trigrams:
        output = " ".join(['TRIGRAM', trigram[0], trigram[1], trigram[2], str(q_values[trigram])])
        outfile.write(output + '\n')
    outfile.close()


# Takes the words from the training data and returns a set of all of the words that occur more than 5 times
# (use RARE_WORD_MAX_FREQ)
# brown_words is a python list where every element is a python list of the words of a particular sentence.
# Note: words that appear exactly 5 times should be considered rare!
def calc_known(brown_words):
    known_words = set([])

    word_count = {}
    for sentence in brown_words:
        for word in sentence:
            if word not in word_count:
                word_count[word] = 1
            else:
                word_count[word] += 1

    for word, count in word_count.items():
        if count > RARE_WORD_MAX_FREQ:
            known_words.add(word)

    return known_words


# Takes the words from the training data and a set of words that should not be replaced for '_RARE_'
# Returns the equivalent to brown_words but replacing the unknown words by '_RARE_' (use RARE_SYMBOL constant)
def replace_rare(brown_words, known_words):
    brown_words_rare = []

    for i, sentence in enumerate(brown_words):
        brown_words_rare.append([])
        for word in sentence:
            if word not in known_words:
                brown_words_rare[i].append(RARE_SYMBOL)
            else:
                brown_words_rare[i].append(word)

    return brown_words_rare


# This function takes the ouput from replace_rare and outputs it to a file
def q3_output(rare, filename):
    outfile = open(filename, 'w', encoding='utf-8')
    for sentence in rare:
        outfile.write(' '.join(sentence[2:-1]) + '\n')
    outfile.close()


# Calculates emission probabilities and creates a set of all possible tags
# The first return value is a python dictionary where each key is a tuple in which the first element is a word
# and the second is a tag, and the value is the log probability of the emission of the word given the tag
# The second return value is a set of all possible tags for this data set
def calc_emission(brown_words_rare, brown_tags):
    e_values = {}
    taglist = set([])

    e_values_count = {}
    tags_count = {}
    for sentence_index in range(len(brown_words_rare)):
        for word_index in range(len(brown_words_rare[sentence_index])):
            current_word = brown_words_rare[sentence_index][word_index]
            current_tag = brown_tags[sentence_index][word_index]
            if (current_word, current_tag) not in e_values_count:
                e_values_count[(current_word, current_tag)] = 1
            else:
                e_values_count[(current_word, current_tag)] += 1

            if current_tag not in tags_count:
                tags_count[current_tag] = 1
            else:
                tags_count[current_tag] += 1

            if brown_tags[sentence_index][word_index] not in taglist:
                taglist.add(brown_tags[sentence_index][word_index])

    for (word, tag), prob in e_values_count.items():
        e_values[(word, tag)] = math.log(float(prob) / tags_count[tag], 2)

    return e_values, taglist


# This function takes the output from calc_emissions() and outputs it
def q4_output(e_values, filename):
    outfile = open(filename, "w", encoding='utf-8')
    emissions = e_values.keys()
    # emissions.sort()  for python 2
    emissions = sorted(emissions)  
    for item in emissions:
        output = " ".join([item[0], item[1], str(e_values[item])])
        outfile.write(output + '\n')
    outfile.close()


# This function takes data to tag (brown_dev_words), a set of all possible tags (taglist), a set of all known words
# (known_words),
# trigram probabilities (q_values) and emission probabilities (e_values) and outputs a list where every element
# is a tagged sentence
# (in the WORD/TAG format, separated by spaces and with a newline in the end, just like our input tagged data)
# brown_dev_words is a python list where every element is a python list of the words of a particular sentence.
# taglist is a set of all possible tags
# known_words is a set of all known words
# q_values is from the return of calc_trigrams()
# e_values is from the return of calc_emissions()
# The return value is a list of tagged sentences in the format "WORD/TAG", separated by spaces.
# Each sentence is a string with a
# terminal newline, not a list of tokens. Remember also that the output should not contain the "_RARE_" symbol,
# but rather the
# original words of the sentence!
def viterbi(brown_dev_words, taglist, known_words, q_values, e_values):
    tagged = []
    pi = {}
    bp = {(-1, START_SYMBOL, START_SYMBOL): START_SYMBOL}
    pi[(-1, START_SYMBOL, START_SYMBOL)] = 0.0

    for sentence in brown_dev_words:
        tokens = []
        for word in sentence:
            if word in known_words:
                tokens.append(word)
            else:
                tokens.append(RARE_SYMBOL)

        # k = 1 case
        if len(tokens) == 1:
            for w in taglist:
                word_tag = (tokens[0], w)
                trigram = (START_SYMBOL, START_SYMBOL, w)
                pi[(0, START_SYMBOL, w)] =\
                    pi[(-1, START_SYMBOL, START_SYMBOL)] +\
                    q_values.get(trigram, LOG_PROB_OF_ZERO) +\
                    e_values.get(word_tag, LOG_PROB_OF_ZERO)
                bp[(0, START_SYMBOL, w)] = START_SYMBOL

        # k = 2 case
        elif len(tokens) == 2:
            for w in taglist:
                for u in taglist:

                    word_tag = (tokens[1], u)
                    trigram = (START_SYMBOL, w, u)
                    pi[(1, w, u)] =\
                        pi.get((0, START_SYMBOL, w), LOG_PROB_OF_ZERO) +\
                        q_values.get(trigram, LOG_PROB_OF_ZERO) +\
                        e_values.get(word_tag, LOG_PROB_OF_ZERO)
                    bp[(1, w, u)] = START_SYMBOL

        # k > 2 case
        else:
            for k in range(2, len(tokens)):
                for u in taglist:
                    for v in taglist:
                        max_prob = float('-Inf')
                        max_tag = ''
                        for w in taglist:
                            score =\
                                pi.get((k - 1, w, u), LOG_PROB_OF_ZERO) +\
                                q_values.get((w, u, v), LOG_PROB_OF_ZERO) +\
                                e_values.get((tokens[k], v), LOG_PROB_OF_ZERO)
                            if score > max_prob:
                                max_prob = score
                                max_tag = w
                        bp[(k, u, v)] = max_tag
                        pi[(k, u, v)] = max_prob

        max_prob = float('-Inf')
        v_max, u_max = None, None
        # finding the max probability of last two tags
        for (u, v) in itertools.product(taglist, taglist):
            score = pi.get((len(sentence) - 1, u, v), LOG_PROB_OF_ZERO) +\
                    q_values.get((u, v, STOP_SYMBOL), LOG_PROB_OF_ZERO)
            if score > max_prob:
                max_prob = score
                u_max = u
                v_max = v
        # append tags in reverse order
        tags = [v_max, u_max]

        for count, k in enumerate(range(len(sentence) - 3, -1, -1)):
            tags.append(bp[(k + 2, tags[count + 1], tags[count])])

        tagged_sentence = []
        # reverse tags
        tags.reverse()
        # stringify tags paired with word without start and stop symbols
        for k in range(0, len(sentence)):
            tagged_sentence += [sentence[k], "/", str(tags[k]), " "]
        tagged_sentence.append('\n')
        tagged.append(''.join(tagged_sentence))

    return tagged


# This function takes the output of viterbi() and outputs it to file
def q5_output(tagged, filename):
    outfile = open(filename, 'w', encoding='utf-8')
    for sentence in tagged:
        outfile.write(sentence)
    outfile.close()


# TODO: IMPLEMENT THIS FUNCTION
# This function uses nltk to create the taggers described in question 6
# brown_words and brown_tags is the data to be used in training
# brown_dev_words is the data that should be tagged
# The return value is a list of tagged sentences in the format "WORD/TAG", separated by spaces.
# Each sentence is a string with a terminal newline, not a list of tokens.
def nltk_tagger(brown_words, brown_tags, brown_dev_words):
    # Hint: use the following line to format data to what NLTK expects for training
    training = [zip(brown_words[i],brown_tags[i]) for i in range(len(brown_words))]

    # IMPLEMENT THE REST OF THE FUNCTION HERE
    tagged = []
    return tagged


# This function takes the output of nltk_tagger() and outputs it to file
def q6_output(tagged, filename):
    outfile = open(filename, 'w', encoding='utf-8')
    for sentence in tagged:
        outfile.write(sentence)
    outfile.close()


def crf(brown_words_rare, brown_tags, brown_dev_words):
    tagged = []
    ct = nltk.CRFTagger()
    train_data = []
    for i in range(len(brown_words_rare)):
        train_data.append([])
        for j in range(len(brown_words_rare[i])):
            train_data[i].append((brown_words_rare[i][j], brown_tags[i][j]))
    ct.train(train_data, 'temp.dat')

    # ct = nltk.CRFTagger()
    # ct.set_model_file('temp.dat')
    tagged_sentences = ct.tag_sents(brown_dev_words)
    for sentence in tagged_sentences:
        tagged_sentence = []
        for word_tag_pair in sentence:
            tagged_sentence += [word_tag_pair[0], "/", word_tag_pair[1], " "]
        tagged_sentence.append('\n')
        tagged.append(''.join(tagged_sentence))
    return tagged


DATA_PATH = 'data/'
OUTPUT_PATH = 'output/'

corpuses_path = {
    'english': {
        'tagged_train': DATA_PATH + 'english/Brown_tagged_train.txt',
        'dev': DATA_PATH + 'english/Brown_dev.txt',
        'tagged_dev': DATA_PATH + 'english/Brown_tagged_dev.txt',
        'hmm_user_tagged_dev': OUTPUT_PATH + 'english/B5_hmm.txt',
        'crf_user_tagged_dev': OUTPUT_PATH + 'english/B5_crf.txt'
    },
    'italian': {
        'source_train': DATA_PATH + 'UD_Italian-master/it-ud-train.conllu',
        'source_dev': DATA_PATH + 'UD_Italian-master/it-ud-dev.conllu',
        'tagged_train': OUTPUT_PATH + 'italian/italian_tagged_train.txt',
        'dev': OUTPUT_PATH + 'italian/italian_dev.txt',
        'tagged_dev': OUTPUT_PATH + 'italian/italian_tagged_dev.txt',
        'hmm_user_tagged_dev': OUTPUT_PATH + 'italian/B5_hmm.txt',
        'crf_user_tagged_dev': OUTPUT_PATH + 'italian/B5_crf.txt'
    },
    'greek': {
        'source_train': DATA_PATH + 'UD_Greek-master/el-ud-train.conllu',
        'source_dev': DATA_PATH + 'UD_Greek-master/el-ud-dev.conllu',
        'tagged_train': OUTPUT_PATH + 'greek/greek_tagged_train.txt',
        'dev': OUTPUT_PATH + 'greek/greek_dev.txt',
        'tagged_dev': OUTPUT_PATH + 'greek/greek_tagged_dev.txt',
        'hmm_user_tagged_dev': OUTPUT_PATH + 'greek/B5_hmm.txt',
        'crf_user_tagged_dev': OUTPUT_PATH + 'greek/B5_crf.txt'
    },
    'gothic': {
        'source_train': DATA_PATH + 'UD_Gothic-master/got-ud-train.conllu',
        'source_dev': DATA_PATH + 'UD_Gothic-master/got-ud-dev.conllu',
        'tagged_train': OUTPUT_PATH + 'gothic/gothic_tagged_train.txt',
        'dev': OUTPUT_PATH + 'gothic/gothic_dev.txt',
        'tagged_dev': OUTPUT_PATH + 'gothic/gothic_tagged_dev.txt',
        'hmm_user_tagged_dev': OUTPUT_PATH + 'gothic/B5_hmm.txt',
        'crf_user_tagged_dev': OUTPUT_PATH + 'gothic/B5_crf.txt'
    },
}


def main():

    for corpus, paths in corpuses_path.items():
        print('### ' + corpus + ' ###')

        # start timer
        time.clock()

        if 'source_train' in paths and 'source_dev' in paths:
            print('Parsing')
            CoNLLParser.parse(paths['source_train'], paths['tagged_train'])
            CoNLLParser.parse(paths['source_dev'], paths['dev'], False)
            CoNLLParser.parse(paths['source_dev'], paths['tagged_dev'])

        # open Brown training data
        infile = open(paths['tagged_train'], "r", encoding='utf-8')
        brown_train = infile.readlines()
        infile.close()

        # split words and tags, and add start and stop symbols (question 1)
        print('Splitting words and tags')
        brown_words, brown_tags = split_wordtags(brown_train)

        # calculate tag trigram probabilities (question 2)
        print('Calculating trigrams')
        q_values = calc_trigrams(brown_tags)

        # question 2 output
        q2_output(q_values, OUTPUT_PATH + corpus + '/B2.txt')

        # calculate list of words with count > 5 (question 3)
        print('Calculating known words')
        known_words = calc_known(brown_words)

        # get a version of brown_words with rare words replace with '_RARE_' (question 3)
        print('Replacing rare words')
        brown_words_rare = replace_rare(brown_words, known_words)

        # question 3 output
        q3_output(brown_words_rare, OUTPUT_PATH + corpus + "/B3.txt")

        # calculate emission probabilities (question 4)
        print('Calculating emission probabilities')
        e_values, taglist = calc_emission(brown_words_rare, brown_tags)

        # question 4 output
        q4_output(e_values, OUTPUT_PATH + corpus + "/B4.txt")

        # delete unneceessary data
        del brown_train

        # open Brown development data (question 5)
        infile = open(paths['dev'], "r", encoding='utf-8')
        brown_dev = infile.readlines()
        infile.close()

        # format Brown development data here
        brown_dev_words = []
        for sentence in brown_dev:
            brown_dev_words.append(sentence.split(" ")[:-1])

        # do viterbi on brown_dev_words (question 5)
        print('Viterbi processing')
        viterbi_tagged = viterbi(brown_dev_words, taglist, known_words, q_values, e_values)

        # question 5 output
        q5_output(viterbi_tagged, OUTPUT_PATH + corpus + '/B5_hmm.txt')

        print('CRF processing')
        crf_tagged = crf(brown_words_rare, brown_tags, brown_dev_words)
        q5_output(crf_tagged, OUTPUT_PATH + corpus + '/B5_crf.txt')

        # print total time to run Part B
        print("Part B time: ", str(time.clock()), ' sec')


if __name__ == "__main__":
    main()
